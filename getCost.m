function meanSquaredDistance = getCost(points, centers)
    no_of_points = size(points, 1);
    no_of_centers = size(centers, 1);
    meanSquaredDistance = 0;
    for i = 1:no_of_points
        point = points(i, :);
        minDistance = realmax;
        for j = 1:no_of_centers
            center = centers(j, :);
            dist = sum((point - center) .^ 2);
            if dist < minDistance
                minDistance = dist;
                % nearestCenter = center;
            end
        end
        meanSquaredDistance = meanSquaredDistance + minDistance;
    end
end
