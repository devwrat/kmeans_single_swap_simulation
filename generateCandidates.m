function candidates = generateCandidates(N, k)
    candidates = randi(N, k, 2);
end