function  optimal = getOptimalSolution(points, k)
    [idx, C] = kmeans(points, k);
    
    optimal = C;
end