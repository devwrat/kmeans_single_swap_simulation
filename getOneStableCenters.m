function oneStableCenters = getOneStableCenters(points, optimalCenters, optimalCost);
    
    k = size(optimalCenters, 1);
    no_of_points = size(points, 1);
    while true
        curCenters = generateCandidates(no_of_points, k);

        curCost = getCost(points, curCenters);
        
        fprintf('alpha^2 = %d\n', curCost / optimalCost);
        
        if curCost / optimalCost < 5 || curCost / optimalCost > 25
            continue;
        end
        
        feasible = true;
        for i = 1:k
            % swapS = curCenters(i, :);
            for j = 1:k
                swapO = optimalCenters(j, :);
                
                newCenters = [curCenters(1:i-1, :); curCenters(i+1:k, :); swapO];
                
                newCost = getCost(points, newCenters);
                if newCost < curCost
                    feasible = false;
                    break
                end
            end
            
            if ~feasible
                break;
            end
        end
        
        if feasible
            oneStableCenters = curCenters;
            break;
        end
    end
end
            