function simulation(N, k)
    points = generatePoints(N, k);
    optimalSolution = getOptimalSolution(points, k);
    optimalCost = getCost(points, optimalSolution);
    oneStable = getOneStableCenters(points, optimalSolution, optimalCost);
    
end