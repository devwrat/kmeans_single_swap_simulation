function points = generatePoints(N, k)

   centers = [];
   
   for i = 1:k
       center1 = randi(N, 1, 2);
       centers = [centers; center1];
   end
   
   points = [];
   for i = 1:k
       sigma = randi(N);
       
       no_of_points = int32(abs(normrnd(0, 2*N/k)));
       
       for j = 1:no_of_points
           point = normrnd(centers(i, :), sigma);
           points = [points; point];
       end
   end
end
   
    

       
   